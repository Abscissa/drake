import drake.all;

// Custom tool
void fooTool(Target t)
{
	// Inserts the contents of t.deps[0] (ex: "foofile") into the executable t.name (ex: "appA").
	// Why? I don't know. Just because.
	// That sort of thing is occasionally needed, though.
	system("fooTool "~t.name~" "~t.deps[0]);
}

// Each array contains a mutually-exclusive set.
// The first in each is considered the default.
immutable modes = [
	["release", "debug"],
	["dmd", "gdc", "ldc"],
	["build", "clean"],
];

immutable dmdArgsRelease = "-wi -release -O -inline";
immutable dmdArgsDebug   = "-wi -debug -g -unittest";

auto getDCompiler()
{
	if(isMode!"dmd")
	{
		if(isMode!"release")
			return &rdmdBuild!"-wi -release -O -inline";
		else
			return &rdmdBuild!"-wi -debug -g -unittest";
	}

	// Etc...
	
	return &noop;
}

// Main func called by Drake
void drakefile()
{
	if(isMode!"clean")
	{
		task("all",
			(Target t)
			{
				// Clean everything
			}
		);
		return;
	}
	
	//if(isMode!"hello") {} // Compile-time error
	
	task("all", ["appA".exe, "appB".exe, "phpApp"]);

	task("upload", "all",
		(Target t)
		{
			system("ftp ...");
		}
	);
	
	/+
	// Abusing opApply would probably make this possible:
	foreach(t; target!Task("upload", "all"))
	{
		system("ftp ...");
	}
	// Not sure that's a good idea though...
	+/
	
	// Needs to be a task so it runs every time.
	// RDMD will handle dependency checking.
	task("appA".exe, ["appA.d", "appAResources.res", "somelib".lib], &rdmdBuild!"-wi");

	// This is considered the same File target as above (because the name is
	// the same), but "foofile" does *not* get sent to rdmdBuild because
	// this is a separate statement. Likewise, the .res and .lib file above
	// don't get sent to fooTool.
	//
	// This is defined after the rdmdBuild, so it's guaranteed to run after rdmdBuild.
	//
	// This must be a Task because it's already a Task above.
	task("appA".exe, "foofile", &fooTool);

	// Dependencies are optional
	file("appAResources.res", 
		(Target t)
		{
			// Make appAResources.res
		}
	);
	
	file("foofile",
		// Generate some data to get crammed into the exe
		(Target t) { touch(t.name); }
	);
	
	task("appB".exe, getDCompiler());
	
	// The output of Haxe/PHP is a whole directory,
	// not just one single file, so this is a Dir target.
	//
	// Also, Haxe finds all its own dependencies, but it *always* rebuilds
	// whether the target needs to be rebuilt or not.
	// So, we'll handle that part with a glob.
	// Instead of a glob, we could have made "phpAppSrc" a separate Dir-type
	// target (with no how-to-build delegate).
	dir("phpApp", "phpApp.hxml"~glob("phpAppSrc/**/*.hx"), &haxe);
	
	// Some fancier stuff:
	task("full-install", "all",
		(Target t)
		{
			auto installDir = DrakeArgs["install-dir"][0];
			copy("appA".exe, installDir);
			copy("appB".exe, installDir);
			copy("phpApp",   installDir~"/phpApp"); // Note, "phpApp" is a dir.
		}
	);

	// A quick install:
	auto installDir = "";
	if(DrakeArgs.target == "quick-install")
	{
		installDir = DrakeArgs["install-dir"][0];
		file(installDir~"/appA".exe, "appA".exe, &copy);
		file(installDir~"/appB".exe, "appB".exe, &copy);
		dir (installDir~"/phpApp",   "phpApp",   &copy);
	}
	
	// "How-to-build" delegate is optional
	task("quick-install", [
		installDir~"/appA".exe,
		installDir~"/appB".exe,
		installDir~"/phpApp"
	]);
}
