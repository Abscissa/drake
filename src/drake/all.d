module drake.all;

public import std.process : system;
import std.stdio;
import drakefile;

abstract class Target
{
	string name;
	string[] deps;
	void delegate(Target) build;
}

/// Task *isn't* assumed to be the name of a file or dir.
/// It's built regardless of whether or not its dependencies are up-to-date.
/// The "all" target is a typical Task.
class Task : Target
{
	//TODO: Implement Task
}
alias target!(Task).target task;

/// File only gets built if its dependencies are out-of-date.
/// Dependency freshness is determined by timestamps.
///
/// Some build systems use a hash of the file's content to determine
/// freshness. Drake doesn't use hashes because "touching" a file without
/// changing it is relativly uncommon and usually indicates a desire to
/// force a rebuild.
///
/// Any dependency (of any type of Target) that's never declared
/// with target!T(...) is implicitly assumed to be a File with no
/// dependencies or build steps of its own.
class File : Target
{
	//TODO: Implement File
}
alias target!(File).target file;

/// Like File, a Dir doesn't get built if its dependencies are up-to-date.
/// Drake considers the timestamp of a Dir to be the timestamp of the most
/// recent modification within the directory (recursively).
///
/// This can be useful, for instance, with a tool like Haxe, which 
/// automatically finds its own dependencies, but doesn't report them.
class Dir : Target
{
	//TODO: Implement Dir
}
alias target!(Dir).target dir;

/// Syntax summary:
/// target(T:Target)( string name, [{string|string[]} deps], [void {delegate|function}(Target) dg}] )
template target(T:Target)
{
	///ditto
	void target(string name, string[] deps=null, void delegate(Target) dg=null)
	{
		//TODO: Register target
	}

	///ditto
	void target(string name, string deps, void delegate(Target) dg=null)
	{
		target(name, [deps], dg);
	}

	///ditto
	void target(string name, void delegate(Target) dg=null)
	{
		target(name, cast(string[])null, dg);
	}

	///ditto
	void target(string name, string[] deps, void function(Target) dg)
	{
		target(name, deps, (Target t){ dg(t); });
	}

	///ditto
	void target(string name, string deps, void function(Target) dg)
	{
		target(name, [deps], dg);
	}

	///ditto
	void target(string name, void function(Target) dg)
	{
		target(name, cast(string[])null, dg);
	}
}

@property string crossPlatformSuffix(string winSuffix, string posixSuffix)(string str)
{
	version(Windows) return str~winSuffix;
	else             return str~posixSuffix;
}

@property string crossPlatformSuffix(string winSuffix, string posixSuffix, string macSuffix)(string str)
{
	version(Windows)  return str~winSuffix;
	else version(OSX) return str~macSuffix;
	else              return str~posixSuffix;
}

alias crossPlatformSuffix!(".exe", ""   ) exe;
alias crossPlatformSuffix!(".bat", ""   ) bat;
alias crossPlatformSuffix!(".bat", ".sh") sh;
alias crossPlatformSuffix!(".obj", ".o" ) obj;
alias crossPlatformSuffix!(".lib", ".a" ) lib;
alias crossPlatformSuffix!(".dll", ".so", ".dylib") slib;

string quote(string str)
{
	version(Windows) return `"`~str~`"`;
	else             return `'`~str~`'`;
}

string quotedJoin(string[] strs)
{
	string ret;
	foreach(str; strs)
	{
		if(ret != "")
			ret ~= " ";
			
		ret ~= str;
	}
	return ret;
}

/// glob("foo/**/*.txt") -->
///   Returns array of all ".txt" files inside directory "foo"
///   and all of "foo"'s subdirectories (recursively).
///   Forward slashes work on all OSes.
string[] glob(string str)
{
	//TODO: Implement glob
	return null;
}

//TODO: Think about some way to generalize the creation of all 2 or 3 versions
//      of each tool below (would also help users define their own tools).

// External tool wrappers that allow &func instead of a function literal:
// (Naturally, configuration params must be compile-time.)
void rdmd(string rdmdArgs="", string appArgs="")(Target t)
{
	rdmd(t.name, t.deps, rdmdArgs, appArgs);
}

void rdmdBuild(string rdmdArgs="")(Target t)
{
	rdmdBuild(t.name, t.deps, rdmdArgs);
}

// External tool wrappers to automatically handle a Target:
void rdmd()(Target t, string rdmdArgs="", string appArgs="")
{
	rdmd(t.name, t.deps, rdmdArgs, appArgs);
}

void rdmdBuild()(Target t, string rdmdArgs="")
{
	rdmdBuild(t.name, t.deps, rdmdArgs);
}

void haxe(Target t)
{
	haxe(t.deps[0]);
}

void touch(Target t)
{
	touch(t.name);
}

void copy(Target t)
{
	copy(t.deps[0], t.name);
}

void noop(Target t)
{
	// Do nothing
}

// Basic external tool wrappers:
/// Source file with main() must be first in deps[].
/// (This may be handled automatically in a later version)
void rdmd()(string targetName, string[] deps, string rdmdArgs="", string appArgs="")
{
	// Move main() file to last
	auto mainSrc = deps[0];
	deps = deps[1..$] ~ mainSrc;

	auto depStr = deps.quotedJoin();
	system("rdmd "~quote("-of:"~targetName)~" "~rdmdArgs~" "~depStr~" "~appArgs);
}

///ditto
void rdmdBuild()(string targetName, string[] deps, string rdmdArgs="")
{
	rdmd(targetName, deps, "--build-only "~rdmdArgs);
}

/// Haxe specifies all its build args in a ".hxml" file
void haxe(string hxmlFile)
{
	system("haxe "~hxmlFile);
}

/// Cross-platform
void touch(string filename)
{
	//TODO: Implement touch
}

/// Cross-platform
void copy(string src, string dest)
{
	//TODO: Implement easy general copy, exact semantics need to be thought about.
}

//TODO: Other typical command line operations like touch and copy.

static class DrakeArgs
{
	static string   target; /// Target the user chose to build
	static string[] modes;  /// Modes the user chose
	
	/// Access the rest of the args.
	///
	/// If an arg is given that isn't built into Drake and is never read
	/// by the drakefile, the user is informed it's an invalid option.
	///
	/// Note that the "invalid option" error will be thrown *before*
	/// any tasks actually get evaluated.
	///
	/// This returns an array to allow multiply-specified args.
	/// This throws a user error if the arg doesn't exist, so the array
	/// returned is guaranteed to have at least one element.
	static string[] opIndex(string name)
	{
		if(name !in misc)
			error("Missing option: --"~name~"={value}");
		
		wasRead[name] = true;
		return misc[name];
	}
	
	/// Check if an arg was given.
	/// Useful for making an arg optional.
	static bool exists(string name)
	{
		return wasRead[name];
	}
	
	private static string[][string] misc;
	private static bool[string] wasRead;
}

class DrakeException : Exception
{
	this(string msg)
	{
		super(msg);
	}
}

template isMode(string mode)
{
	//TODO: Implement isMode
	enum isMode = false;
}

void error(string msg)
{
	throw new DrakeException(msg);
}

//TODO: Verify "modes[]" is ok.
//TODO: Verify "drakefile()" is ok.
//TODO: Grab modes and misc args from command line.
//TODO: Support alternate filenames for "drakefile.d"
//TODO: Support --force to assume all deps are out-of-date

static if( !__traits(compiles, { drakefile.drakefile(); }) )
{
	pragma(msg, "Your drakefile must define the func 'void drakefile()'.");
	pragma(msg, "Additionally, the module name of your drakefile must be 'drakefile'.");
	static assert(false);
}

static if( !__traits(compiles, { auto x = modes; }) )
{
	immutable string[] modes = [];
}

private int main(string[] args)
{
	args = args[1..$]; // Skip exe name
	
	if(args.length >= 1)
		DrakeArgs.target = args[0];
	
	try
		drakefile.drakefile();
	catch(DrakeException e)
	{
		writeln(e.msg);
		return 1;
	}
	
	//TODO: Perform build.
	
	return 0;
}
