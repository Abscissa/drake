@echo off

echo If this doesn't work, you've forgotten one or more of these:
echo.
echo 1. Set the DRAKE environment var:
echo    SET DRAKE=full_path_to_main_drake_directory_here
echo.
echo 2. You must have RDMD from DMD 2.054 or later. The RDMD
echo    from DMD 2.053 and earlier has bugs and will not work.
echo.
echo 3. Your active DMD compiler must be D2, and your drakefile
echo    must be written in D2. But you can still use drake to build a D1
echo    project: Just install DVM, and in your drakefile, run DVM's
echo    dmd-{ver} shortcuts.

rdmd "-I%DRAKE%\src" "%DRAKE%\src\drake\all.d" %*
